#ifndef UTIL_H
#define UTIL_H

#include <glib.h>

GList *gnome_cups_hash_table_keys (GHashTable *hash);
GList *gnome_cups_hash_table_values (GHashTable *hash);

#endif
